import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { connect } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
// import AuthScreen from './src/screens/AuthScreen';
// import TimezoneListScreen from './src/screens/TimezoneListScreen';
// import UserListScreen from './src/screens/UserListScreen';
// import UserEditScreen from './src/screens/UserEditScreen';
import Main from './src/components/Main';

class App extends Component {

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk))
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default App;
