import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  SET_USER_ROLE,
  SAVE_ME_SUCCESS,
}
from '../actions/types';

const INITIAL_STATE= {
  token: '',
  role: '',
  error: '',
  loading: false,
  me: null
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case LOGIN_USER:
      return { ...state, loading: true, error: '', me: null }
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, token: action.payload };
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication failed', loading: false };
    case SET_USER_ROLE:
      return { ...state, role: action.payload.role, me: action.payload };
    case SAVE_ME_SUCCESS:
      return { ...state, me: null};
    default:
      return state;
  }
}
