import { combineReducers } from 'redux';
import SignInReducer from './SignInReducer';
import SignUpReducer from './SignUpReducer';
import TimezonesReducer from './TimezonesReducer';
import UsersReducer from './UsersReducer';

export default combineReducers({
  signIn: SignInReducer,
  signUp: SignUpReducer,
  timezones: TimezonesReducer,
  users: UsersReducer,
})
