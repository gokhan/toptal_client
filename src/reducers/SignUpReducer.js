import {
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
  SIGNUP_USER,
}
from '../actions/types';

const INITIAL_STATE= {
  role: '',
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case SIGNUP_USER:
      return { ...state, loading: true, error: '' }
    case SIGNUP_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, error: action.payload};
    case SIGNUP_USER_FAIL:
      console.log('SIGNUP_USER_FAIL: ', action);
      return { ...state, error: JSON.stringify(action.payload), loading: false };
    default:
      return state;
  }
}
