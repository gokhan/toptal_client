import {
  FETCH_TIMEZONES_SUCCESS,
  FETCH_TIMEZONES_FAIL,
  EDIT_TIMEZONE,
  SAVE_TIMEZONE_SUCCESS,
  SAVE_TIMEZONE_FAIL,
  DELETE_TIMEZONE_SUCCESS,
  SET_SEARCH_KEYWORD,
  LOGIN_USER_SUCCESS,
}
from '../actions/types';

const INITIAL_STATE= {
  data: [],
  error: '',
  loading: false,
  item: null,
  reload: false,
  keyword: ''
};


export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case LOGIN_USER_SUCCESS:
      return { ...state, data: []};
    case SET_SEARCH_KEYWORD:
      return { ...state, keyword: action.payload, reload: true };
    case FETCH_TIMEZONES_SUCCESS:
      return { ...state, ...INITIAL_STATE, data: action.payload };
    case FETCH_TIMEZONES_FAIL:
      return { ...state, error: 'Authentication failed', loading: false };
    case EDIT_TIMEZONE:
      return { ...state, item: action.payload };
    case SAVE_TIMEZONE_SUCCESS:
        return { ...state, error: '', item: null, reload: true };
    case DELETE_TIMEZONE_SUCCESS:
        return { ...state, error: '', item: null, reload: true };
    case SAVE_TIMEZONE_FAIL:
        return { ...state, error: JSON.stringify(action.payload), reload: false, loading: false };
    default:
      return state;
  }
}
