import {
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAIL,
  EDIT_USER,
  SAVE_USER_SUCCESS,
  SAVE_USER_FAIL,
  DELETE_USER_SUCCESS,
  EDIT_ME,
}
from '../actions/types';

const INITIAL_STATE= {
  data: [],
  error: '',
  loading: false,
  item: null,
  me: null,
  reload: false,
};


export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case FETCH_USERS_SUCCESS:
      return { ...state, ...INITIAL_STATE, data: action.payload };
    case FETCH_USERS_FAIL:
      return { ...state, error: 'Authentication failed', loading: false };
    case EDIT_USER:
      return { ...state, item: action.payload };
    case EDIT_ME:
      return { ...state, me: action.payload, reload: true };
    case SAVE_USER_SUCCESS:
        return { ...state, error:'', item: null, me: null, reload: true };
    case DELETE_USER_SUCCESS:
        return { ...state, error: '', item: null, reload: true };
    case SAVE_USER_FAIL:
        return { ...state, error: JSON.stringify(action.payload), reload: false, loading: false };
    default:
      return state;
  }
}
