import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';

import Timezones from '../components/Timezones';

class TimezoneListScreen extends Component {
  static navigationOptions =  {
            title: 'Timezones',
            // tabBarIcon: ({ tintColor }) => <Icon name="access-time" size={35} color={tintColor} />,
            tabBarIcon: ({ tintColor }) => <Icon name='clock-o' type='font-awesome' />,
          }
  onTimezonePress = (item) => {
    this.props.navigation.navigate('timezoneEdit');
  }

  render() {
    const {navigate} = this.props.navigation;

    return(
      <View style={styles.container}>
        <Timezones onTimezonePress={this.onTimezonePress} navigation={navigate}/>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#ecf0f1',
    marginTop:20
  },
};

export default TimezoneListScreen;
