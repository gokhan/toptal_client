import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editMe } from '../actions';
import { View, Text } from 'react-native';
import { Icon, Button } from 'react-native-elements';
import UserEdit from '../components/UserEdit';

class UserEditScreen extends Component {
  static navigationOptions = {
            tabBarLabel: 'Edit',
            tabBarIcon: ({ tintColor }) => <Icon name='cogs' type='font-awesome' />,
          }


  render() {
    const {navigate} = this.props.navigation;

    return(
      <View style={styles.container}>
        <UserEdit navigation={this.props.navigation}/>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#ecf0f1',
    marginTop:20,
  },
};

const mapStateToProps = ( { users } ) => {
  const { error, item } = users
  return { error, item };
}

export default connect(mapStateToProps, { editMe })(UserEditScreen);
