import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { logout } from '../actions';

import SignInForm from '../components/SignInForm';
import SignUpForm from '../components/SignUpForm';

class AuthScreen extends Component {
  static navigationOptions =  {
            title: 'Logout',
            tabBarVisible: false,
            tabBarIcon: ({ tintColor }) => <Icon name='sign-out' type='font-awesome' />,
          }

  render() {
    return(
      <View style={styles.container}>
        <SignUpForm />
        <SignInForm />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    marginTop:20
  },
};

export default AuthScreen;
