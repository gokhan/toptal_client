import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import Users from '../components/Users';

class UserListScreen extends Component {
  static navigationOptions = {
            tabBarLabel: 'Users',
            tabBarIcon: ({ tintColor }) => <Icon name='users' type='font-awesome' />,
          }

  render() {
    return(
      <View style={styles.container}>
        <Users />
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#ecf0f1',
    marginTop:20
  },
};
export default UserListScreen;
