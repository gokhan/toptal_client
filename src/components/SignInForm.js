import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { loginUser } from '../actions';
import { FormLabel, FormInput, Button, Text } from 'react-native-elements';

const ROOT_URL = "http://localhost:3000";

class SignInForm extends Component {
  state = { email: 'admin@example.com', password: 'admin'};

  onButtonPress(){
    const {email, password} = this.state;

    this.props.loginUser({ email, password });
  }

  render() {
    return(
      <View style={styles.container}>
        <Text h2>Sign in</Text>
        <Text style={styles.error}>{this.props.error}</Text>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Email</FormLabel>
          <FormInput
            value={this.state.email}
            onChangeText={email => this.setState({email})}
          />
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Password</FormLabel>
          <FormInput
            secureTextEntry
            onChangeText={password => this.setState({password})}
            value={this.state.password}
          />
        </View>
        <Button
          iconRight
          title='Submit'
          icon={{name: 'check'}}
          backgroundColor='#3b5998'
          onPress={this.onButtonPress.bind(this)}
          />


      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  error: {
    color: 'red'
  }
};

const mapStateToProps = ( { signIn } ) => {
  const { error, loading } = signIn
  return { error, loading };
}

export default connect(mapStateToProps, {loginUser})(SignInForm);
