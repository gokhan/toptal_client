import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { createTimezone, updateTimezone, deleteTimezone, cancelTimezoneEdit } from '../actions';
import { FormLabel, FormInput, Button, Text } from 'react-native-elements';

class TimezoneEdit extends Component {
  state = { id: null, name: '', city: '', difference: '0'};

  componentDidMount(){
    const{ id, name, city, difference } = this.props.item
    this.setState({id, name, city, difference});
  }

  onButtonPress(){
    const {id, name, city, difference } = this.state;
    if(id){
      this.props.updateTimezone(id, name, city, difference);
    }else{
      this.props.createTimezone(name, city, difference);
    }
  }

  onButtonDelete = () => {
    this.props.deleteTimezone(this.state.id);
  }

  renderCancelButton(){
    return(
      <Button
          title='Go back'
          raised
          style={styles.button}
          icon={{name: 'cancel'}}
          onPress={this.clickCancel}
          />
    )
  }

  clickCancel = () => {
    this.props.cancelTimezoneEdit();
  }

  render() {
    return(
      <View style={styles.container}>
        <Text h2>Timezone edit</Text>
        <View style={{flexDirection:'row'}}>
          <Text style={styles.error}>{this.props.error}</Text>
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Name</FormLabel>
          <FormInput
            value={this.state.name}
            onChangeText={name => this.setState({name})}
          />
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>City</FormLabel>
          <FormInput
            onChangeText={city => this.setState({city})}
            value={this.state.city}
          />
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Difference</FormLabel>
          <FormInput
            onChangeText={difference => this.setState({difference})}
            value={this.state.difference}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
              raised
              icon={{name: 'cached'}}
              title='Submit'
              style={styles.button}
              backgroundColor='blue'
              onPress={this.onButtonPress.bind(this)}
              />
          {this.renderCancelButton()}
          {this.renderDeleteButton()}
        </View>
      </View>

    )
  }
  renderDeleteButton(){
    if(this.state.id){
      return(
        <Button
            title='Delete'
            raised
            style={styles.button}
            icon={{name: 'delete'}}
            backgroundColor='red'
            onPress={this.onButtonDelete}
            />
      )
    }
  }
}


const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: 'flex-start',
    // justifyContent: 'center',
    // backgroundColor: '#ecf0f1',
  },
  buttonContainer: {
    flexDirection: 'row',
    height: 40,
    width:120
  },
  button: {
    flex: 1,
  },
  error: {
    color: 'red',
    flexWrap: "wrap"
  }
};

const mapStateToProps = ( { timezones } ) => {
  const { error, item } = timezones
  return { error, item };
}

export default connect(mapStateToProps, {createTimezone, updateTimezone, deleteTimezone, cancelTimezoneEdit})(TimezoneEdit);
