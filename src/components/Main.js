import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import reducers from '../reducers';
import { TabNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

// Screens
import AuthScreen from '../screens/AuthScreen';
import TimezoneListScreen from '../screens/TimezoneListScreen';
import UserListScreen from '../screens/UserListScreen';
import UserEditScreen from '../screens/UserEditScreen';

class Main extends Component {

  renderAdmin(){
    const AdminNavigator = TabNavigator({
      timezonesScreen: { screen: TimezoneListScreen },
      userList: { screen: UserListScreen },
      userEdit: { screen: UserEditScreen },
      authScreen: { screen: AuthScreen },
    });
    return (
      <AdminNavigator />
    );
  }

  renderManager(){
    const ManagerNavigator = TabNavigator({
      timezonesScreen: {
        screen: TimezoneListScreen
      },
      userList: {
        screen: UserListScreen
       },
      userEdit: {
        screen: UserEditScreen
      },
      authScreen: { screen: AuthScreen },

    },{headerMode: 'screen'});
    return (
      <ManagerNavigator />
    );
  }

  renderUser(){
    const UserNavigator = TabNavigator({
      timezonesScreen: { screen: TimezoneListScreen },
      userEdit: { screen: UserEditScreen },
      authScreen: { screen: AuthScreen },
    });

    return (
      <UserNavigator />
    );
  }

  render() {
    if(this.props.role === ''){
      return (
        <View style={styles.container}>
          <AuthScreen />
        </View>
      );
    }

    // Signed-in user based on role we show navigations
    if (this.props.role === 'admin'){
      return this.renderAdmin();
    }else if (this.props.role === 'manager'){
      return this.renderManager();
    }else{
      return this.renderUser();
    }


  }
}
const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: '#ecf0f1',
    marginTop:20
  },
};

const mapStateToProps = ( { signIn } ) => {
  const { role } = signIn
  return { role };
}
export default connect(mapStateToProps)(Main);
