import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { signUpUser } from '../actions';
import axios from 'axios';
import { FormLabel, FormInput, Button, Text } from 'react-native-elements';


class SignUpForm extends Component {
  state = { email: '', password: 'user'};

  onButtonPress(){
    const {email, password} = this.state;

    this.props.signUpUser({ email, password });
  }

  render() {
    return(
      <View style={styles.container}>
        <Text h2>Sign up</Text>
        <Text style={styles.error}>{this.props.error}</Text>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Email</FormLabel>
          <FormInput
            value={this.state.email}
            onChangeText={email => this.setState({ email })}
          />
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Password</FormLabel>
          <FormInput
            secureTextEntry
            value={this.state.password}
            onChangeText={password => this.setState({ password })}
          />
        </View>
        <Button
          iconRight
          title='Submit'
          icon={{name: 'check'}}
          backgroundColor='#3b5998'
          onPress={this.onButtonPress.bind(this)}
          />
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  error: {
    color: 'red'
  }
};


const mapStateToProps = ( { signUp } ) => {
  const { error, loading } = signUp
  return { error, loading };
}

export default connect(mapStateToProps, {signUpUser})(SignUpForm);
