import React, { Component } from 'react';
import { View, AsyncStorage, FlatList, TouchableOpacity } from 'react-native';
import { List, ListItem } from "react-native-elements";
import { connect } from 'react-redux';
import { fetchUsers, editUser } from '../actions';
import { FormLabel, FormInput, Button, Text, SearchBar, Header, Grid, Col } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import UserEdit from './UserEdit';

class Users extends Component {
  state = { item: null};

  componentDidMount(){
    this.props.fetchUsers();
  }

  renderHeader = () => {
    return(
          <Grid>
              <Col size={3}>
                <Button
                  title=''
                  backgroundColor='green'
                  icon={{name: 'plus', type: 'font-awesome'}}
                  style={{marginTop:4}}
                  onPress={() => this.props.editUser({id: null, email: '', password: ''})}
                  />
              </Col>
            </Grid>
          );
  };


  render() {
    if (this.props.reload){
      this.props.fetchUsers();
    }

    if(this.props.item){
      return(
        <UserEdit item={this.props.item} deleteButton='true' goTo={this.goTo}/>
      )
    }

    return(
      <List  style={{ backgroundColor: 'white', alignSelf: "stretch" }}>
        <FlatList
          ListHeaderComponent={this.renderHeader}
          data={this.props.data}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => this.props.editUser(item)}>

            <ListItem
              title={`${item.email}`}
            />
            </TouchableOpacity>
          )}
        />
      </List>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  error: {
    color: 'red'
  }
};

const mapStateToProps = ( { users } ) => {
  const { data, item, reload} = users
  return { data, item, reload };
}

export default connect(mapStateToProps, {fetchUsers, editUser})(Users);
