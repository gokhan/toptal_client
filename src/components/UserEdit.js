import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { fetchUsers, createUser, updateUser, deleteUser, editMe, resetEdit } from '../actions';
import { FormLabel, FormInput, Button, Text } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

class UserEdit extends Component {
  state = { id: null, email: '', password: ''};

  componentDidMount(){
    if(this.props.item){
      const{ id, email, password} = this.props.item;
      this.setState({id, email, password});
    }else if(this.props.me){
      const{ id, email, password} = this.props.me;
      this.setState({id, email, password});
    }
  }

  async onButtonPress(){
    const {id, email, password } = this.state;
    if(id){
      await this.props.updateUser(id, email, password);
      if(this.props.error === '')
        this.clickCancel();
    }else{
      this.props.createUser(email, password);
    }
  }

  onButtonDelete = () => {
    this.props.deleteUser(this.state.id);
  }

  render() {
    return(
      <View style={styles.container}>
        <Text h2 style={{ marginLeft:18}}>User edit</Text>
        <View style={{flexDirection:'row'}}>
          <Text style={styles.error}>{this.props.error}</Text>
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Email</FormLabel>
          <FormInput
            value={this.state.email}
            onChangeText={email => this.setState({email})}
          />
        </View>
        <View style={{ marginBottom: 10 }} >
          <FormLabel>Password</FormLabel>
          <FormInput
            secureTextEntry
            onChangeText={password => this.setState({password})}
            value={this.state.password}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button
              raised
              iconRight
              icon={{name: 'cached'}}
              title='Submit'
              style={styles.button}
              backgroundColor='blue'
              onPress={this.onButtonPress.bind(this)}
              />
          {this.cancelButton()}
          {this.deleteButton()}
        </View>
      </View>

    )
  }

  clickCancel = () => {
    this.props.resetEdit();
    if(this.props.navigation){
      const navigateAction = NavigationActions.navigate({
        routeName: 'userList',
        params: {},
      })
      this.props.navigation.dispatch(navigateAction)
    }
  }

  cancelButton(){
    return(
      <Button
          title='Go back'
          raised
          style={styles.button}
          icon={{name: 'cancel'}}
          onPress={this.clickCancel}
          />
    )
  }
  deleteButton(){
    if (this.props.deleteButton === 'true'){
      if(this.state.id){
        return(
          <Button
              title='Delete'
              raised
              style={styles.button}
              icon={{name: 'delete'}}
              backgroundColor='red'
              onPress={this.onButtonDelete}
              />
        )
      }
    }
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonContainer: {
    flexDirection: 'row',
    height: 40
  },
  button: {
    flex: 1,
  },
  error: {
    color: 'red',
    flexWrap: "wrap"
  }
};

const mapStateToProps = ( { users, signIn } ) => {
  const { error, item } = users
  const { me } = signIn;
  return { error, item, me };
}

export default connect(mapStateToProps, {fetchUsers, createUser, updateUser, deleteUser, editMe, resetEdit})(UserEdit);
