import React, { Component } from 'react';
import { View, AsyncStorage, FlatList, TouchableOpacity } from 'react-native';
import { List, ListItem } from "react-native-elements";
import { connect } from 'react-redux';
import { fetchTimezones, editTimezone, setSearchKeyword } from '../actions';
import { FormLabel, FormInput, Button, Text, SearchBar, Header, Grid, Col } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import TimezoneEdit from './TimezoneEdit';

class Timezones extends Component {
  state = { item: null};

  fetchTimezones(){
    this.props.fetchTimezones(this.props.keyword);
  }

  componentDidMount(){
    this.fetchTimezones();
  }

  searchTimezones = (keyword) => {
    this.props.setSearchKeyword(keyword);
  }

  renderHeader = () => {
    return(
          <Grid>
              <Col size={9}>
                <SearchBar
                      placeholder="Search Timezones by name"
                      lightTheme
                      textInputRef='keyword'
                      onChangeText={keyword => this.searchTimezones(keyword)}
                    />
              </Col>
              <Col size={3}>
                <Button
                  title=''
                  backgroundColor='green'
                  icon={{name: 'plus', type: 'font-awesome'}}
                  style={{marginTop:4}}
                  onPress={() => this.props.editTimezone({id: null, name: '', city: ''})}
                  />
              </Col>
            </Grid>
          );
  };

  render() {
    if (this.props.reload){
      this.fetchTimezones();
    }
    const timezones = this.props.data.map(function (timezone){
        return (
            <Text key={timezone.id}>{timezone.name}, {timezone.city} ({timezone.difference})</Text>
        );
    });
    if(this.props.item){
      return(
        <TimezoneEdit item={this.props.item}/>
      )
    }

    return(
      <List  style={{ backgroundColor: 'white', alignSelf: "stretch" }}>
        <FlatList
          ListHeaderComponent={this.renderHeader}
          data={this.props.data}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => this.props.editTimezone(item)}>

            <ListItem
              title={`${item.name} (${item.city})`}
              subtitle={`Difference: ${item.difference} (Current: ${item.current})`}
            />
            </TouchableOpacity>
          )}
        />
      </List>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  error: {
    color: 'red'
  }
};

const mapStateToProps = ( { timezones } ) => {
  const { data, item, reload, keyword } = timezones
  return { data, item, reload, keyword };
}

export default connect(mapStateToProps, {fetchTimezones, editTimezone, setSearchKeyword})(Timezones);
