import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../consts';

import {
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAIL,
  EDIT_USER,
  SAVE_USER_SUCCESS,
  SAVE_USER_FAIL,
  DELETE_USER_SUCCESS,
  EDIT_ME,
  SAVE_ME_SUCCESS,
  LOGOUT,
} from './types';

export const resetEdit = () => {
  return (dispatch) => {
    dispatch({
      type: EDIT_ME,
      payload: null
    });
  }
}


export const fetchUsers =  () => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;
      let response = await axios.get( `${API_URL}/api/users` );
      fetchSuccess(dispatch, response.data);
    }
    catch(err) {
      fetchFail(dispatch);
    }
  };
};


export const createUser = ( email, password ) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.post(`${API_URL}/api/users`, {
        user: { email, password}
      });

      if (response.data.error){
        saveFail(dispatch, response.data.error);
      }else{
        await fetchUsers();
        saveSuccess(dispatch);
      }
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};

export const deleteUser = ( id ) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.delete(`${API_URL}/api/users/${id}`);
      deleteSuccess(dispatch);
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};


export const editUser = (item) =>{
  return (dispatch) => {
    dispatch({
      type: EDIT_USER,
      payload: item
    });
  }
}


export const editMe = () =>{
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.get(`${API_URL}/api/users/me`);
      dispatch({
        type: EDIT_ME,
        payload: response.data
      });
    }
    catch(err) {
    }
  };
}

export const updateUser = (id, email, password ) => {
  console.log('update user');
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;
      let response = await axios.patch(`${API_URL}/api/users/${id}`, {
        user: {email, password }
      });
      if (response.data.error){
        saveFail(dispatch, response.data.error);
      }else{
        saveSuccess(dispatch);
        // await fetchUsers();
      }
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};


const fetchSuccess = (dispatch, users) => {
  dispatch({
    type: FETCH_USERS_SUCCESS,
    payload: users
  });
}

const fetchFail = (dispatch) => {
  dispatch({
    type: FETCH_USERS_FAIL
  });
}

const saveSuccess = (dispatch) => {
  dispatch({
    type: SAVE_USER_SUCCESS
  });
}

const deleteSuccess = (dispatch) => {
  dispatch({
    type: DELETE_USER_SUCCESS
  });
}

const saveFail = (dispatch, error) => {
  dispatch({
    type: SAVE_USER_FAIL,
    payload: error
  });
}
