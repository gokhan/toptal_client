import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../consts';

import {
  FETCH_TIMEZONES_SUCCESS,
  FETCH_TIMEZONES_FAIL,
  EDIT_TIMEZONE,
  SAVE_TIMEZONE_SUCCESS,
  SAVE_TIMEZONE_FAIL,
  DELETE_TIMEZONE_SUCCESS,
  SET_SEARCH_KEYWORD
} from './types';

export const cancelTimezoneEdit = () => {
  return (dispatch) => {
    deleteSuccess(dispatch);
  };
}

export const setSearchKeyword =  (keyword) => {
  return (dispatch) => {
    dispatch({
      type: SET_SEARCH_KEYWORD,
      payload: keyword
    });
  };
};

export const fetchTimezones =  (keyword) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;
      let url = ''
      if (keyword == ''){
        url = `${API_URL}/api/timezones`;
      }else{
        url = `${API_URL}/api/timezones/search`;
      }
      let response = await axios.get( url, {params: {keyword: keyword}} );
      fetchSuccess(dispatch, response.data);
    }
    catch(err) {
      fetchFail(dispatch);
    }
  };
};

export const createTimezone = (name, city, difference ) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.post(`${API_URL}/api/timezones`, {
        timezone: { name, city, difference }
      });
      saveSuccess(dispatch, response.data);
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};

export const deleteTimezone = ( id ) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.delete(`${API_URL}/api/timezones/${id}`);
      deleteSuccess(dispatch);
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};


export const editTimezone = (item) =>{
  console.log(item)
  return (dispatch) => {
    dispatch({
      type: EDIT_TIMEZONE,
      payload: item
    });
  }
}

export const updateTimezone = (id, name, city, difference ) => {
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;
      let response = await axios.patch(`${API_URL}/api/timezones/${id}`, {
        timezone: {name, city, difference }
      });
      if (response.data.error){
        saveFail(dispatch, response.data.error);
      }else{
        await fetchTimezones();
        saveSuccess(dispatch);
      }
    }
    catch(err) {
      saveFail(dispatch, err);
    }
  };
};


const fetchSuccess = (dispatch, timezones) => {
  dispatch({
    type: FETCH_TIMEZONES_SUCCESS,
    payload: timezones
  });
}

const fetchFail = (dispatch) => {
  dispatch({
    type: FETCH_TIMEZONES_FAIL
  });
}

const saveSuccess = (dispatch) => {
  dispatch({
    type: SAVE_TIMEZONE_SUCCESS
  });
}

const deleteSuccess = (dispatch) => {
  dispatch({
    type: DELETE_TIMEZONE_SUCCESS
  });
}

const saveFail = (dispatch, error) => {
  dispatch({
    type: SAVE_TIMEZONE_FAIL,
    payload: error
  });
}
