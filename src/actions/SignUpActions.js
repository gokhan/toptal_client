import axios from 'axios';
import { Actions } from 'react-native-router-flux';

import{ API_URL } from '../consts';

import {
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAIL,
  SIGNUP_USER,
} from './types';


export const signUpUser =  ({ email, password }) => {
  return async (dispatch) => {
    dispatch({ type: SIGNUP_USER });
    try{
      let response = await axios.post(`${API_URL}/api/users`, { user: { email, password } });
      const { meta } = response.data;
      console.log(response.data);
      if(response.data.error){
        signUpUserFail(dispatch, response.data.error);
      }else{
        signUpUserSuccess(dispatch);
      }
    }
    catch(err){
    }
  };
};


const signUpUserFail = (dispatch, error) => {
  dispatch({
    type: SIGNUP_USER_FAIL,
    payload: error
  });

}
const signUpUserSuccess = (dispatch) => {
  dispatch({
    type: SIGNUP_USER_SUCCESS,
    payload: 'You have successfully signed up'
  });
}
