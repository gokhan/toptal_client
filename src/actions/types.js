export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const SET_USER_ROLE = 'set_user_role';
export const SIGNUP_USER_SUCCESS = 'sign_up_success';
export const SIGNUP_USER_FAIL = 'sign_up_fail';
export const SIGNUP_USER = 'sign_up_user';
export const FETCH_TIMEZONES_SUCCESS = 'fetch_timezones_success';
export const FETCH_TIMEZONES_FAIL = 'fetch_timezones_fail';
export const SAVE_TIMEZONE_SUCCESS = 'save_timezones_success';
export const SAVE_TIMEZONE_FAIL = 'save_timezones_fail';
export const EDIT_TIMEZONE = 'edit_timezone';
export const DELETE_TIMEZONE_SUCCESS = 'delete_timezone_success';
export const SET_SEARCH_KEYWORD = 'set_search_keyword';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_FAIL = 'FETCH_USERS_FAIL';
export const EDIT_USER = 'EDIT_USER';
export const SAVE_USER_SUCCESS = 'SAVE_USER_SUCCESS';
export const SAVE_USER_FAIL = 'SAVE_USER_FAIL';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const EDIT_ME = 'EDIT_ME';
export const SAVE_ME_SUCCESS = 'SAVE_ME_SUCCESS';
export const LOGOUT = 'logout';
