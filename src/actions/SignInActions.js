import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';

import{ API_URL } from '../consts';

import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  SET_USER_ROLE
} from './types';


export const loginUser =  ({ email, password }) => {
  return async (dispatch) => {
    dispatch({ type: LOGIN_USER });
    try{
      let response = await axios.post(`${API_URL}/api/authenticate`, { auth: { email, password } });
      await AsyncStorage.setItem('token', response.data.jwt);
      loginUserSuccess(dispatch, response.data.jwt);
      fetchUserRole(dispatch, response.data.jwt);
    }
    catch(err) {
      loginUserFail(dispatch);
    }
  };
};

const editMe = () =>{
  return async (dispatch) => {
    try{
      let token =  await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = token;

      let response = await axios.get(`${API_URL}/api/users/me`);
      dispatch({
        type: EDIT_ME,
        payload: response.data
      });
    }
    catch(err) {
    }
  };
}

const fetchUserRole = async (dispatch, token) => {
  let response =  await axios.get(`${API_URL}/api/users/me`);
  setUserData(dispatch, response.data);
};

const loginUserFail = (dispatch) => {
  dispatch({
    type: LOGIN_USER_FAIL
  });
}
const loginUserSuccess = (dispatch, token) => {
  axios.defaults.headers.common['Authorization'] = token;
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: token
  });
}

const setUserData = (dispatch, user) => {
  dispatch({
    type: SET_USER_ROLE,
    payload: user
  });
}
